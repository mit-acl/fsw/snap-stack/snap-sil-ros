/**
 * @file snap_sil_ros_node.cpp
 * @brief Entry point for ROS snap stack simulation
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 18 December 2019
 */

#include <iostream>

#include <ros/ros.h>

#include "snap_sil_ros/snap_sil_ros.h"

int main(int argc, char* argv[])
{
  // Get sim type
  if (argc < 2) {
    std::cout << "Missing sim_type command-line arg: ie 'flightgoggles'" << std::endl;
    return 1;
  }
  const std::string sim_type(argv[1]);

  ros::init(argc, argv, "snap_sil_ros_node");
  acl::snap_sil_ros::SnapSilRos quad(sim_type);

  // Wait a bit before ROS interface running
  ros::Duration(1.0).sleep();

  ros::spin();
  return 0;
}
