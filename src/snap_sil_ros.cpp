/**
 * @file snap_sil_ros.cpp
 * @brief ROS wrapper for ACL multirotor dynamic simulation
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 23 December 2019
 */

#include <std_msgs/Empty.h>
#include <std_srvs/SetBool.h>
#include <mav_msgs/Actuators.h>

#include <unistd.h>

#include "snap_sil_ros/snap_sil_ros.h"

namespace acl {
namespace snap_sil_ros {

SnapSilRos::SnapSilRos(const std::string& sim_type)
: nhp_("~") {
  // Check namespace to find name of vehicle
  name_ = ros::this_node::getNamespace();
  size_t n = name_.find_first_not_of('/');
  name_.erase(0, n); // remove leading slashes
  if (name_.empty()) {
    ROS_ERROR("Error :: You should be using a launch file to specify the "
              "node namespace!\n");
    ros::shutdown();
    return;
  }

  // Handle some stuff specific to the sim we're using
  if (sim_type.compare("flightgoggles") == 0) {
    pub_cmd_ = nh_.advertise<mav_msgs::Actuators>("actuators", 0);
  } else {
    ROS_ERROR_STREAM("Error :: Unrecognized sim type " << sim_type << ". Quitting.");
    ros::shutdown();
    return;
  }

  // Command rate
  nhp_.param<double>("cmd_dt", cmd_dt_, 0.005);

  // Number of rotors
  nhp_.param<int>("num_rotors", num_rotors_, 4);

  // Register ROS interface
  sub_imu_ = nh_.subscribe("imu", 1, &SnapSilRos::imuPassthrough, this, ros::TransportHints().unreliable());
  sub_reset_ = nh_.subscribe("reset_signal_in", 1, &SnapSilRos::resetCallback, this);

  //
  // SIL Communications
  //
  const size_t imukey = acl::ipc::createKeyFromStr(name_, "imu");
  const size_t esckey = acl::ipc::createKeyFromStr(name_, "esc");

  // unique key is used to access the same shmem location
  imuserver_.reset(new acl::ipc::Server<sensor_imu>(imukey));
  escclient_.reset(new acl::ipc::Client<esc_commands>(esckey));

  //
  // ESC ``Read'' thread
  //
  escthread_ = std::thread(&SnapSilRos::escReadThread, this);

  //
  // Motor command publish based on ROS timer
  //
  tim_cmd_ = nh_.createTimer(ros::Duration(cmd_dt_), &SnapSilRos::cmdTimerCallback, this);

  //
  // Auto-arming
  //
  nhp_.param<bool>("autoarm", auto_arm_, false);
  if (auto_arm_) {
    // This guy *originates* the arming command
    pub_arm_ = nh_.advertise<std_msgs::Empty>("arm", 1, true);
    arm_request_active_ = true;
    arm_request_setpoint_ = true;
  } else {
    // Set up callback to listen for arming signal
    sub_arm_ = nh_.subscribe("arm", 1, &SnapSilRos::armCallback, this);
  }
  tim_arm_ = nh_.createTimer(ros::Duration(0.2), &SnapSilRos::armTimerCallback, this);
}

// ----------------------------------------------------------------------------

SnapSilRos::~SnapSilRos()
{
  if (escthread_.joinable()) escthread_.join();
}

// ----------------------------------------------------------------------------
// Arming thread
// ----------------------------------------------------------------------------
void SnapSilRos::armTimerCallback(const ros::TimerEvent&)
{
  if (arm_request_active_) {
    const bool success = call_snap_arming_service();

    if (success) {
      // If we are responsible for publishing arm messages on the ROS end, do so now
      if (pub_arm_ && arm_request_setpoint_ == true) {
        pub_arm_.publish(std_msgs::Empty());
      }

      // Trigger re-arm if auto-arm is on (this is part of a reset cycle)
      if (auto_arm_ && arm_request_setpoint_ == false) {
        std::cout << "snap_sil_ros: Triggering auto re-arm." << std::endl;
        arm_request_active_ = true;
        arm_request_setpoint_ = true;
      }
    }
  }
}

void SnapSilRos::armCallback(const std_msgs::Empty::ConstPtr&) {
  if (arm_request_setpoint_ == false) {
    arm_request_active_ = true;
    arm_request_setpoint_ = true;

    // Try calling immediately
    call_snap_arming_service();
  }
}

void SnapSilRos::resetCallback(const std_msgs::Empty::ConstPtr&) {
  arm_request_active_ = true;
  arm_request_setpoint_ = false;
}

bool SnapSilRos::call_snap_arming_service() {
  // If we have an active request to arm
  auto srv_arm = nh_.serviceClient<std_srvs::SetBool>("snap/arm");

  // wait until snap stack connects
  srv_arm.waitForExistence();

  // create request to arm
  std_srvs::SetBool rqt;
  rqt.request.data = arm_request_setpoint_;

  // Multiple tries may be necessary since arming is not allowed until the IMU has been calibrated.
  srv_arm.call(rqt);
  //if (rqt.response.success) {  // snap service handler seems to fill ``success'' in with setpoint value rather than actual success
  if (rqt.response.success == arm_request_setpoint_) {
    if (arm_request_setpoint_ == true) {
      //ROS_INFO("snap_sil_ros: ARM successful.");
      std::cout << "snap_sil_ros: ARM successful." << std::endl;
    } else {
      //ROS_INFO("snap_sil_ros: DISARM successful.");
      std::cout << "snap_sil_ros: DISARM successful." << std::endl;;
    }

    // Set arm request inactive
    arm_request_active_ = false;

    return true;
  }

  // Otherwise, failure
  return false;
}


// ----------------------------------------------------------------------------

void SnapSilRos::escReadThread()
{
  static constexpr uint16_t PWM_MAX = acl::ESCInterface::PWM_MAX_PULSE_WIDTH;
  static constexpr uint16_t PWM_MIN = acl::ESCInterface::PWM_MIN_PULSE_WIDTH;

  while (ros::ok()) {
    esc_commands esccmds;
    bool rcvd = escclient_->read(&esccmds);

    if (rcvd) {
      std::lock_guard<std::mutex> lck(escmtx_);
      motorcmds_.resize(num_rotors_);
      for (size_t i=0; i<num_rotors_; ++i)
        motorcmds_[i] = static_cast<double>(esccmds.pwm[i] - PWM_MIN) / (PWM_MAX - PWM_MIN);
    }

    // Sleep for 0.02ms
    usleep(20);
  }
}

void SnapSilRos::cmdTimerCallback(const ros::TimerEvent& e) {
  // Publish actuator output
  // TODO: This currently assumes we're using FlightGoggles,
  // need to add some logic here if generalizing this to other sims.
  mav_msgs::Actuators act_msg;
  act_msg.header.stamp = ros::Time::now();
  act_msg.header.frame_id = name_;

  if (motorcmds_.size() != num_rotors_) {
    // Command zero by default
    act_msg.normalized.resize(num_rotors_, 0.);
  } else {
    // Output normalized commands
    act_msg.normalized = motorcmds_;
  }
  pub_cmd_.publish(act_msg);
}

// ----------------------------------------------------------------------------

void SnapSilRos::imuPassthrough(const sensor_msgs::Imu::ConstPtr& msg)
{
  imu_.timestamp_in_us = msg->header.stamp.toNSec()/1000;
  imu_.sequence_number++;
  // Rotate the measurements so that they correspond to the IMU orientation
  // of the sf board (eagle8074). Further, note that IMU measures in g's.
  static constexpr float g = 9.80665f;
  imu_.linear_acceleration[0] =   msg->linear_acceleration.x / g;
  imu_.linear_acceleration[1] = - msg->linear_acceleration.y / g;
  imu_.linear_acceleration[2] = - msg->linear_acceleration.z / g;
  imu_.angular_velocity[0] =   msg->angular_velocity.x;
  imu_.angular_velocity[1] = - msg->angular_velocity.y;
  imu_.angular_velocity[2] = - msg->angular_velocity.z;
  imuserver_->send(imu_);
}

} // ns snap_sil_ros
} // ns acl
