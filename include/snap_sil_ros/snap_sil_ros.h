/**
 * @file snap_sil_ros.h
 * @brief ROS wrapper for ACL multirotor dynamic simulation
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 23 December 2019
 */

#pragma once

#include <memory>
#include <mutex>
#include <string>
#include <thread>

#include <ros/ros.h>

#include <std_srvs/SetBool.h>
#include <std_msgs/Empty.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <sensor_msgs/Imu.h>

#include "client.h"
#include "server.h"
#include "ipc_common.h"
#include "sensor-imu/sensor_datatypes.h"
#include "esc_interface/esc_datatypes.h"
#include "esc_interface/esc_interface.h"

namespace acl {
namespace snap_sil_ros {

class SnapSilRos
{
public:
  explicit SnapSilRos(const std::string& sim_type = "flightgoggles");  ///< So far FlightGoggles is only one supported
  ~SnapSilRos();

private:
  ros::NodeHandle nh_, nhp_;
  ros::Timer tim_cmd_;
  ros::Publisher pub_cmd_, pub_arm_;
  ros::Subscriber sub_imu_, sub_reset_, sub_arm_;

  // Arming logic
  bool auto_arm_;  // If true, will try to arm automatically
  bool arm_request_active_ = false;  // if we're in the process of requesting an arm status
  bool arm_request_setpoint_ = false;  // to-be-set
  ros::Timer tim_arm_;
  void armTimerCallback(const ros::TimerEvent&);
  void armCallback(const std_msgs::Empty::ConstPtr&);
  void resetCallback(const std_msgs::Empty::ConstPtr&);
  bool call_snap_arming_service();

  /// \brief Parameters
  std::string name_; ///< name of the simulated vehicle
  double cmd_dt_; ///< command period
  int num_rotors_;

  /// \brief Internal state
  sensor_imu imu_; ///< imu data message
  std::vector<double> motorcmds_; ///< pwm commands btwn 1000 and 2000 usec

  /// \brief Components
  std::unique_ptr< ipc::Server<sensor_imu> > imuserver_;
  std::unique_ptr< ipc::Client<esc_commands> > escclient_;

  /// \brief Thread for reading esc data
  std::thread escthread_;
  std::mutex escmtx_;

  void escReadThread();
  void cmdTimerCallback(const ros::TimerEvent& e);
  void imuPassthrough(const sensor_msgs::Imu::ConstPtr& msg);
};

} // ns snap_sil_ros
} // ns acl
